//
//  NewsListVC.swift
//  NewsApp
//
//  Created by next on 26/10/22.
//

import UIKit
import SDWebImage
class NewsListVC: UIViewController {
    
    @IBOutlet weak var readMoreButton: UIButton!
    @IBOutlet weak var newsTableView: UITableView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var newsList:[ArticleVM]?
    var visibleItemCount:Int = 0
    var selectedIndex:Int?
    var newsListViewModel:NewsListViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.getNews()
    }

    private func initialSetup() {
        self.configureUI()
    }
    
    private func configureUI() {
        self.title = "News"
        self.configureNewsTableView()
        self.configureActivityIndicator()
        self.configureReadMoreButton()
    }
    
    private func configureNewsTableView(){
        self.newsTableView.delegate = self
        self.newsTableView.dataSource = self
        self.newsTableView.backgroundColor = .clear
        self.newsTableView.separatorColor = .clear
        self.newsTableView.showsVerticalScrollIndicator = false
        self.newsTableView.estimatedRowHeight = 300
        let noDataLabel = UILabel()
        noDataLabel.text = "No Data Available"
        noDataLabel.textColor = .black
        noDataLabel.textAlignment = .center
        noDataLabel.isHidden = true
        self.newsTableView.backgroundView = noDataLabel
    }
    
    private func configureActivityIndicator() {
        self.activityIndicator.style = .large
        self.showActivityIndicator(show: false)
    }
    private func configureReadMoreButton(){
        self.readMoreButton.addTarget(self, action: #selector(readMoreButtonAction), for: .touchUpInside)
        self.readMoreButton.layer.cornerRadius = 12
        self.readMoreButton.clipsToBounds = true
    }
    private func showActivityIndicator(show:Bool) {
        self.activityIndicator.isHidden = !show
        if show{
            self.activityIndicator.startAnimating()
        }
        else{
            self.activityIndicator.stopAnimating()
        }
    }
    
    private func showRetryAlert(errorMessage:String) {
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Retry", style: .default) { _ in
            self.getNews()
        }
        alert.addAction(retryAction)
        self.present(alert, animated: true)
    }
    
    @objc private func readMoreButtonAction(){
        self.visibleItemCount += 2
        self.newsTableView.reloadData()
        self.newsTableView.scrollToRow(at: IndexPath(row: self.visibleItemCount-1, section: 0), at: .bottom, animated: true)
        self.readMoreButton.isHidden = self.visibleItemCount == self.newsList?.count
    }
}

//MARK: - API Calls
extension NewsListVC:NewsListViewModelProtocol{
    private func getNews() {
        self.showActivityIndicator(show: true)
        self.newsListViewModel = NewsListViewModel(delegate: self)
        self.newsListViewModel?.getNewsList()
    }
    
    func updateNewsResponse(news: NewsResponseVM?, error: String?) {
        self.showActivityIndicator(show: false)
        if let error = error {
            self.showRetryAlert(errorMessage: error)
        }
        else{
            self.visibleItemCount = 2
            self.newsTableView.backgroundView?.isHidden = !(news?.articles?.isEmpty == true)
            self.newsList = news?.articles
            self.newsTableView.reloadData()
            
        }
    }
    
    private func getNewsReactionDetails(cell:NewsTableViewCell?,index:Int,reactionTypes:[ReactionType]){
        guard let urlString = self.newsList?[index].url else {
            self.newsList?[index].reactionDataState.likeState = .error
            self.newsList?[index].reactionDataState.commentState = .error
            return
        }
        self.newsListViewModel?.getNewsReactionDetails(reactionTypes: [.like,.comment], articleStringURL: urlString)
        if self.newsList?[index].reactionDataState.likeState == ReactionState.loading && reactionTypes.contains(.like){
            cell?.configureReaction(reactionState: .loading, reactionType: .like, reactionCount: nil)
        }
        
        if self.newsList?[index].reactionDataState.commentState == ReactionState.loading && reactionTypes.contains(.comment){
            cell?.configureReaction(reactionState: .loading, reactionType: .comment, reactionCount: nil)
        }
    }
    
    func updateNewsReactionResponse(newsReaction: Reaction?, error: String?) {
        guard let index = self.newsList?.firstIndex(where: {$0.url == newsReaction?.articleStringURL}) else {return}
        guard let reactionType = newsReaction?.reactionType else {
            return
        }
        let count:Int? = newsReaction?.reactionType == .like ? newsReaction?.likes : newsReaction?.comments
        let cell = self.newsTableView.cellForRow(at: IndexPath(row: index, section: 0)) as? NewsTableViewCell
        cell?.configureReaction(reactionState: error == nil ? .success : .error, reactionType: reactionType, reactionCount: count)
        
        if reactionType == .like{
            self.newsList?[index].reactionDataState.likeState = error == nil ? .success : .error
            self.newsList?[index].likeCount = count
        }
        else{
            self.newsList?[index].reactionDataState.commentState = error == nil ? .success : .error
            self.newsList?[index].commentCount = count
        }
        
        if index == self.selectedIndex {
            (self.navigationController?.viewControllers.last as? NewsDetailVC)?.updateAndRefreshReactionUI(newsItem: self.newsList?[index])
        }
    }
}

//MARK: - TableView Methods
extension NewsListVC:UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.newsList?.count ?? Int.zero
        return self.visibleItemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellID = String(describing: NewsTableViewCell.self)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? NewsTableViewCell else {return UITableViewCell()}
        cell.configureCell(newsItem: self.newsList?[indexPath.row],index:indexPath.row,delegate:self)
        self.getNewsReactionDetails(cell: cell, index:indexPath.row, reactionTypes: [.like,.comment])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailVC = self.storyboard?.instantiateViewController(withIdentifier: String(describing: NewsDetailVC.self)) as? NewsDetailVC else {return}
        detailVC.newsItem = self.newsList?[indexPath.row]
        detailVC.delegate = self
        self.selectedIndex = indexPath.row
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: - TableViewCell Delegete methods
extension NewsListVC:NewsTableViewCellProtocol {
    func getNewsResctionDetails(index: Int, reactionType: ReactionType) {
        let cell:NewsTableViewCell? = self.newsTableView.cellForRow(at: IndexPath(row: index, section: 0)) as? NewsTableViewCell
        if reactionType == .like{
            self.newsList?[index].reactionDataState.likeState = .loading
        }
        else{
            self.newsList?[index].reactionDataState.commentState = .loading
        }
        self.getNewsReactionDetails(cell: cell, index: index, reactionTypes: [reactionType])
        if index == self.selectedIndex {
            (self.navigationController?.viewControllers.last as? NewsDetailVC)?.updateAndRefreshReactionUI(newsItem: self.newsList?[index])
        }
    }
}

//MARK: - DetailVC Delegate Methods
extension NewsListVC:NewsDetailVCProtocol{
    func retryButtonTapped(newsItem:ArticleVM?,reactionType:ReactionType){
        guard let index = self.newsList?.firstIndex(where: {$0.url == newsItem?.url}) else {return}
        self.getNewsResctionDetails(index: index, reactionType: reactionType)
    }
}
