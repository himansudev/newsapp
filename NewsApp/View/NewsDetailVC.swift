//
//  NewsDetailVC.swift
//  NewsApp
//
//  Created by next on 27/10/22.
//

import UIKit
protocol NewsDetailVCProtocol{
    func retryButtonTapped(newsItem:ArticleVM?,reactionType:ReactionType)
}
class NewsDetailVC: UIViewController {
    var delegate:NewsDetailVCProtocol?
    var newsItem:ArticleVM?
    @IBOutlet weak var likeActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var commentActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var commentCountCumRetryButton: UIButton!
    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var likeCountCumRetryButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    private func initialSetup() {
        self.configureUI()
    }
}

//MARK: - Configuring UI
extension NewsDetailVC{
    private func configureUI(){
        self.view.backgroundColor = .white
        self.configureAuthorLabel()
        self.configureTitleLabel()
        self.configureDescriptionLabel()
        self.configureArticleImageView()
        self.configureReactionUI()
    }
    
    private func configureAuthorLabel(){
        let author = self.newsItem?.author == nil ? "Unknown Author" : newsItem!.author!
        self.authorLabel.text = "By : " + author
        self.authorLabel.font = UIFont.systemFont(ofSize: 11)
        self.authorLabel.numberOfLines = 0
    }
    
    private func configureTitleLabel(){
        self.titleLabel.text = self.newsItem?.title
        self.titleLabel.isHidden = self.newsItem?.title == nil
        self.titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        self.titleLabel.numberOfLines = 0
    }
    
    private func configureDescriptionLabel() {
        self.descriptionLabel.text = newsItem?.description
        self.descriptionLabel.isEnabled = self.newsItem?.description == nil
        self.descriptionLabel.numberOfLines = 0
    }
    private func configureArticleImageView() {
        articleImageView.backgroundColor = .gray
        articleImageView.layer.cornerRadius = 12
        articleImageView.clipsToBounds = true
        articleImageView.contentMode = .scaleToFill
        if let imageStringURL = self.newsItem?.imageURL,
           let imageURL = URL(string: imageStringURL){
            articleImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named:"placeholderImage"))
        }
        else{
            articleImageView.image = UIImage(named:"placeholderImage")
        }
    }
}

//MARK: - Reaction UI
extension NewsDetailVC{
    private func configureReactionUI(){
        self.likeCountCumRetryButton.addTarget(self, action: #selector(retryButtonAction(_:)), for: .touchUpInside)
        self.commentCountCumRetryButton.addTarget(self, action: #selector(retryButtonAction(_:)), for: .touchUpInside)
        
        self.configureReaction(reactionState: self.newsItem?.reactionDataState.likeState, reactionType: .like, reactionCount: self.newsItem?.likeCount)
        self.configureReaction(reactionState: self.newsItem?.reactionDataState.commentState, reactionType: .comment, reactionCount: self.newsItem?.commentCount)
    }

    private func showReactionLoader(reactions:[ReactionType]?,show:Bool) {
        guard let reactions = reactions else {
            return
        }
        reactions.forEach({ reaction in
            switch reaction{
            case .like:
                self.likeActivityIndicator.isHidden = !show
                if show{
                    self.likeActivityIndicator.startAnimating()
                }
                else{
                    self.likeActivityIndicator.stopAnimating()
                }
                
            case .comment:
                self.commentActivityIndicator.isHidden = !show
                if show{
                    self.commentActivityIndicator.startAnimating()
                }
                else{
                    self.commentActivityIndicator.stopAnimating()
                }
            }
        })
    }
    
    private func configureReaction(reactionState:ReactionState?,reactionType:ReactionType, reactionCount:Int?) {
        switch reactionType {
        case .like:
            self.likeCountCumRetryButton.isHidden = reactionState == .loading
            self.likeImageView.isHidden = reactionState == .loading
            self.likeCountCumRetryButton.setTitleColor(reactionState == .success ? UIColor.blue : UIColor.red, for: .normal)
            self.likeCountCumRetryButton.setTitle(reactionState == .success ? "(\(reactionCount ?? 0))" : "Retry", for: .normal)
            self.likeImageView.tintColor = reactionState == .success ? .blue : .red
            self.likeImageView.image = UIImage(systemName: "hand.thumbsup.fill")
            self.likeCountCumRetryButton.isUserInteractionEnabled = reactionState == .error
        case .comment:
            self.commentCountCumRetryButton.isHidden = reactionState == .loading
            self.commentImageView.isHidden = reactionState == .loading
            self.commentCountCumRetryButton.setTitleColor(reactionState == .success ? UIColor.blue : UIColor.red, for: .normal)
            self.commentCountCumRetryButton.setTitle(reactionState == .success ? "(\(reactionCount ?? 0))" : "Retry", for: .normal)
            self.commentImageView.tintColor = reactionState == .success ? .blue : .red
            self.commentImageView.image = UIImage(systemName: "text.bubble.fill")
            self.commentCountCumRetryButton.isUserInteractionEnabled = reactionState == .error
            
        }
        self.showReactionLoader(reactions: [reactionType], show: reactionState == .loading)
    }
    
    @objc private func retryButtonAction(_ sender:UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            let reactionType:ReactionType = sender == self.likeCountCumRetryButton ? .like : .comment
            self.delegate?.retryButtonTapped(newsItem: self.newsItem, reactionType: reactionType)
        }
    }
    
    func updateAndRefreshReactionUI(newsItem:ArticleVM?){
        self.newsItem = newsItem
        self.configureReactionUI()
    }
}

enum ReactionState{
    case loading
    case success
    case error
}
