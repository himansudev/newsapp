//
//  NewsTableViewCell.swift
//  NewsApp
//
//  Created by next on 27/10/22.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
//    var reactionDataState:ReactionState = .loading
    var delegate:NewsTableViewCellProtocol?
    @IBOutlet weak var newsImageView:UIImageView!
    @IBOutlet weak var newsDescriptionLabel:UILabel!
    @IBOutlet weak var newsAuthorLabel:UILabel!
    @IBOutlet weak var cardBackgroundView: UIView!
    
    @IBOutlet weak var likeActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var likeCountCumRetryButton: UIButton!
    
    @IBOutlet weak var commentCountCumRetryButton: UIButton!
    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var commentActivityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialSetup()
    }
    
    
    private func initialSetup(){
        self.backgroundColor = .clear
        
        self.selectionStyle = .none
        
        cardBackgroundView.layer.cornerRadius = 14
        cardBackgroundView.clipsToBounds = true
        
        newsImageView.backgroundColor = .black
        newsImageView.layer.cornerRadius = 12
        newsImageView.clipsToBounds = true
        //It seems all the images received from server is same as image view aspect ratio we can user scaleAspectFill otherwise can use scaleAspectFit if that is not the case
        newsImageView.contentMode = .scaleAspectFill
        
        //Since it is list screen and detail screen is present for the same, it is not necessary to show all data in list screen
        newsDescriptionLabel.numberOfLines = 5
        newsDescriptionLabel.font = UIFont.systemFont(ofSize: 14)
        
        newsAuthorLabel.font = UIFont.systemFont(ofSize: 11)
        self.configureReactionUI()
    }
    
    func configureCell(newsItem:ArticleVM?,index:Int,delegate:NewsTableViewCellProtocol) {
        self.delegate = delegate
        self.tag = index
        self.newsDescriptionLabel.text = newsItem?.description
        let author = ((newsItem?.author == nil) || (newsItem?.author?.isEmpty == true)) ? "Unknown Author" : newsItem!.author!
        self.newsAuthorLabel.text =  "By : " + author
        if let imageStringURL = newsItem?.imageURL,
           let imageURL = URL(string: imageStringURL){
            self.newsImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named:"placeholderImage"))
            self.newsImageView.contentMode = .scaleAspectFill
        }
        else{
            self.newsImageView.image = UIImage(named:"placeholderImage")
            self.newsImageView.contentMode = .scaleToFill
        }
    }
}

//MARK: - Reaction UI
extension NewsTableViewCell{
    private func configureReactionUI(){
        self.likeCountCumRetryButton.addTarget(self, action: #selector(retryButtonAction(_:)), for: .touchUpInside)
        self.commentCountCumRetryButton.addTarget(self, action: #selector(retryButtonAction(_:)), for: .touchUpInside)
        self.configureReaction(reactionState: .success, reactionType: .like, reactionCount: nil)
        self.configureReaction(reactionState: .success, reactionType: .comment, reactionCount:nil)
    }

    private func showReactionLoader(reactions:[ReactionType]?,show:Bool) {
        guard let reactions = reactions else {
            return
        }
        reactions.forEach({ reaction in
            switch reaction{
            case .like:
                self.likeActivityIndicator.isHidden = !show
                if show{
                    self.likeActivityIndicator.startAnimating()
                }
                else{
                    self.likeActivityIndicator.stopAnimating()
                }
                
            case .comment:
                self.commentActivityIndicator.isHidden = !show
                if show{
                    self.commentActivityIndicator.startAnimating()
                }
                else{
                    self.commentActivityIndicator.stopAnimating()
                }
            }
        })
    }
    
    func configureReaction(reactionState:ReactionState,reactionType:ReactionType, reactionCount:Int?) {
        switch reactionType {
        case .like:
            self.likeCountCumRetryButton.isHidden = reactionState == .loading
            self.likeImageView.isHidden = reactionState == .loading
            self.likeCountCumRetryButton.setTitleColor(reactionState == .success ? UIColor.blue : UIColor.red, for: .normal)
            self.likeCountCumRetryButton.setTitle(reactionState == .success ? "(\(reactionCount ?? 0))" : "Retry", for: .normal)
            self.likeImageView.tintColor = reactionState == .success ? .blue : .red
            self.likeImageView.image = UIImage(systemName: "hand.thumbsup.fill")
            self.likeCountCumRetryButton.isUserInteractionEnabled = reactionState == .error
        case .comment:
            self.commentCountCumRetryButton.isHidden = reactionState == .loading
            self.commentImageView.isHidden = reactionState == .loading
            self.commentCountCumRetryButton.setTitleColor(reactionState == .success ? UIColor.blue : UIColor.red, for: .normal)
            self.commentCountCumRetryButton.setTitle(reactionState == .success ? "(\(reactionCount ?? 0))" : "Retry", for: .normal)
            self.commentImageView.tintColor = reactionState == .success ? .blue : .red
            self.commentImageView.image = UIImage(systemName: "text.bubble.fill")
            self.commentCountCumRetryButton.isUserInteractionEnabled = reactionState == .error
            
        }
        self.showReactionLoader(reactions: [reactionType], show: reactionState == .loading)
    }
    
    @objc private func retryButtonAction(_ sender:UIButton) {
        let reactionType:ReactionType = sender == self.likeCountCumRetryButton ? .like : .comment
        self.delegate?.getNewsResctionDetails(index: self.tag, reactionType: reactionType)
    }
}

protocol NewsTableViewCellProtocol{
    func getNewsResctionDetails(index:Int,reactionType:ReactionType)
}
