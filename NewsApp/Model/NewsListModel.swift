//
//  Entity.swift
//  NewsApp
//
//  Created by next on 26/10/22.
//

import Foundation

struct NewsResponse: Decodable {
    let status: String?
    let totalResults: Int?
    let articles: [Article]?
}


struct Article: Decodable {
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
}


struct ArticleVM{
    let author: String?
    let title: String?
    let description: String?
    let imageURL:String?
    let url:String?
    var likeCount:Int?
    var commentCount:Int?
    var reactionDataState:ReactionDataState = ReactionDataState()
    static func convertToArticleVM(_ articles:[Article]?) -> [ArticleVM]?{
        guard let articles = articles else {
            return nil
        }
        return articles.map {
            ArticleVM.init(author: $0.author, title: $0.title, description: $0.description, imageURL: $0.urlToImage, url: $0.url)
        }
    }
}

struct NewsResponseVM{
    let status: String?
    let totalResults: Int?
    let articles: [ArticleVM]?
    
    static func convertIntoUIModel(response:NewsResponse?) -> NewsResponseVM?{
        guard let response = response else {
            return nil
        }
        let articles = ArticleVM.convertToArticleVM(response.articles)
        return NewsResponseVM(status: response.status, totalResults: response.totalResults, articles: articles)
    }
}
struct ReactionDataState{
    var likeState:ReactionState = .loading
    var commentState:ReactionState = .loading
}

struct Reaction:Decodable{
    let likes:Int?
    let comments:Int?
    var reactionType:ReactionType?
    var articleStringURL:String?
    
    enum CodingKeys: String, CodingKey {
           case likes, comments
       }
}

enum ReactionType:String{
    case like
    case comment
}
