//
//  NewsListViewModel.swift
//  NewsApp
//
//  Created by next on 27/10/22.
//

import Foundation
struct NewsListViewModel:NewsInteractorProtocol{
    let delegate:NewsListViewModelProtocol
    func getNewsList() {
        let interactor = NewsListInteractor(delegate: self)
        interactor.getNewsList()
    }
    
    func updateNewsResponse(error: String?, newsResponse: NewsResponseVM?) {
        self.delegate.updateNewsResponse(news: newsResponse, error: error)
    }
    
    func getNewsReactionDetails(reactionTypes:[ReactionType],articleStringURL:String) {
        let interactor = NewsListInteractor(delegate: self)
        for reactionType in reactionTypes{
            interactor.getNewsReactionDetails(reactionType:reactionType,articleStringURL: articleStringURL)
        }
    }
    
    func updateNewsReactionResponse(error: String?, newsReaction:Reaction?) {
        self.delegate.updateNewsReactionResponse(newsReaction:newsReaction, error: error)
    }
}

protocol NewsListViewModelProtocol{
    func updateNewsResponse(news:NewsResponseVM?, error:String?)
    func updateNewsReactionResponse(newsReaction:Reaction? ,error:String?)
}
