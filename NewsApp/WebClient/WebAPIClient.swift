//
//  WebAPIClient.swift
//  NewsApp
//
//  Created by next on 27/10/22.
//


import Foundation
struct WebAPIClient
{
    func getRequest<T:Decodable>(requestUrl: URL, resultType: T.Type, completionHandler:@escaping(_ result: T?,_ error:String?)-> Void)
    {
        if Reachability.isConnectedToNetwork{
            URLSession.shared.dataTask(with: requestUrl) { (responseData, httpUrlResponse, error) in
                if let error = error {
                    completionHandler(nil, error.localizedDescription)
                }
                else {
                    if let responseData = responseData {
                        let decoder = JSONDecoder()
                        do {
                            let result = try decoder.decode(T.self, from: responseData)
                            completionHandler(result,nil)
                        }
                        catch let error{
                            debugPrint("error occured while decoding = \(error.localizedDescription)")
                            completionHandler(nil, error.localizedDescription)
                        }
                    }
                    else {
                        completionHandler(nil, Constants.SOMETHING_WENT_WRONG)
                    }
                }
            }.resume()
        }
    }
}

struct Reachability{
    static var isConnectedToNetwork:Bool{
        return true
    }
}
