//
//  Common.swift
//  NewsApp
//
//  Created by next on 27/10/22.
//

import Foundation
struct ApiEndpoints{
    static private let apiKey:String = "bf3c83e35b6f45619e0dc2ac9d302787"
    static let baseURL:String = "https://newsapi.org"
    static let getNewsList:String = ApiEndpoints.baseURL + "/v2/top-headlines?country=us&apiKey=\(ApiEndpoints.apiKey)"
    
    static let likeCommentBaseURL:String = "https://cn-news-info-api.herokuapp.com"
    static let getLikes:String = Self.likeCommentBaseURL + "/likes"
    static let getComments:String = Self.likeCommentBaseURL + "/comments"
}

struct Constants{
    static let SOMETHING_WENT_WRONG = "Something went wrong"
}
