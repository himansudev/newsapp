//
//  NewsInteractor.swift
//  NewsApp
//
//  Created by next on 26/10/22.
//

import Foundation

struct NewsListInteractor{
    
    let delegate:NewsInteractorProtocol
    
    func getNewsList() {
        let apiClient:WebAPIClient = WebAPIClient()
        guard let url = URL(string: ApiEndpoints.getNewsList) else {return}
        apiClient.getRequest(requestUrl: url, resultType: NewsResponse.self) { result, error in
            DispatchQueue.main.async {
                
                self.delegate.updateNewsResponse(error: error, newsResponse: NewsResponseVM.convertIntoUIModel(response: result))
            }
        }
    }
    
    func getNewsReactionDetails(reactionType:ReactionType,articleStringURL:String) {
        let apiClient:WebAPIClient = WebAPIClient()
        
        let urlString = ((reactionType == .like) ? ApiEndpoints.getLikes : ApiEndpoints.getComments) + "/\(articleStringURL.replacingOccurrences(of: "/", with: "_"))"
        
        guard let url = URL(string: urlString) else {return}
        apiClient.getRequest(requestUrl: url, resultType: Reaction.self) { result, error in
            DispatchQueue.main.async {
                var reactionDetails = result
                reactionDetails?.reactionType = reactionType
                reactionDetails?.articleStringURL = articleStringURL
                self.delegate.updateNewsReactionResponse(error: error, newsReaction: reactionDetails)
            }
        }
    }
}

protocol NewsInteractorProtocol{
    func updateNewsResponse(error:String?, newsResponse:NewsResponseVM?)
    func updateNewsReactionResponse(error:String?, newsReaction:Reaction?)
}
